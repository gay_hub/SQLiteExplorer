#include "mainwindow.h"
#include <QApplication>
#include <QFileInfo>
#include <set>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.resize(1600, 1000);
    w.show();

    if (argc > 1)
    {
        auto path = argv[1];
        QFileInfo fileInfo(path);
        std::set<QString> validSuffix = {
            "db","sqlite"
        };
        if(fileInfo.isFile() && validSuffix.count(fileInfo.suffix().toLower()) > 0)
        {
            w.openDatabaseFile(path);
        }
    }

    return a.exec();
}

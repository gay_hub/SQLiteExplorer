TARGET   = sqlite3
TEMPLATE = lib
#DESTDIR  = $$PWD/../bin
CONFIG(debug, debug|release) {
DESTDIR  = $$PWD/../debug
} else {
DESTDIR  = $$PWD/../release
}
#message($$PWD)

msvc {
    DEFINES += SQLITE_ENABLE_COLUMN_METADATA
    DEF_FILE = sqlite3.def
    QMAKE_CFLAGS += /utf-8
    QMAKE_CXXFLAGS += /utf-8
}

HEADERS = sqlite3.h

SOURCES = sqlite3.c
